# Decouverte en quelques commandes de GIT


## Afficher les fichier masqué 
### Windows
Clic droit : afficher les fichier masqué

### Mac
Dans le terminal ecrire :
    Pour afficher les fichiers :
        ```
        defaults write com.apple.finder AppleShowAllFiles TRUE
        ```
    Pour fermer le finder (relance necessaire)
        ```
        killall Finder
        ```
    Masquer les fichiers cachés 
        ```
        defaults write com.apple.finder AppleShowAllFiles FALSE
        ```
    Pour fermer le finder (relance necessaire)
        ```
        killall Finder
        ```

## init
Permet d'initialiser un projet GIT vide à partir du dossier courant.
```
git init
```

## status
Permet de vérifier l'état des fichier à un instant t
```
git status
```

## add
Permet d'ajouter un ou des fichiers à l'index
```
git add <nom_du_fichier> [<Si_besoin : Nom des autres fichier si besoin séparé avec un espace>]
```

## commit
Permet de sauvegarder à un instant t notre projet
<message> : Doit être le plus court et le plus explicite possible et en Anglais !
```
git commit -m "message"
```
Si on rajoute -am permet d'ajouter tout les fichier + le message derriere

## log/plog1/plog2
Permet d'afficher l'historique des commit effectués
log affiche en ligne -> visuel pas top
```
git log
```
plog1 et plog2 sont plus visuel 
```
git plog1
git plog2
```


## push

Permet d'envoyer les modification commitées du dépot local sur le dépot distant (équivalent à uploader le projet)
```
git push
```

## clone
Permet de récupérer un dépot distant

## Creer un projet sur GitLab

- New Project
- Rentrer les informations

## Cloner un projet GIT sur mon ordi
Ouvrir le terminal au dossier

### command Git clone
Permet de cloner un projet, dans Git on clique sur Clone et on récupère l'URL HTTPS du projets et dans le terminal :
```
git clone url_https_du_projet
```

## Ouvrir terminal sur Visual studio code
Terminal
new terminal
Choisir pour quel dossier
S'assurer que Bash soit écrit dans la liste déroulante à droite (si ce n'ets pas le cas, cliqué sur Select Default Shell et choisir bash)


## Enregistrer les modification d'un projet sur GIT
Ouvrir le terminal du projet 

-> Selectionner les fichiers
Pour un seul fichier :

```
git add nom_du_fichier.extension
```
Pour tous les fichier

```
git add -A
```

-> Vérifier que le ou les fichiers sont bien pres

```
git status
```
Il(s) doit(vent) apparaitre en vert

-> Mettre l'indication

```
git commit -m "message"
```

-> Le pousser

```
git push
```
-> Verifier

```
git log
```

## Renitialiser l'author au commit

```
git add nom_du_fichier.extension
git commit -m "message"
git config user.name "afougeray221"
git config user.email "afougeray221@gmail.com"
git commit --amend --reset-author
:x
```

derriere quand on fait un git log on voit que le nom de l'author à changé

## :x
correspond à enregistrer et quitter (quand one est bloqué)

## merge
Permet de fusionner une branche dans une autre branche.
Vous devez vous positionner sur la branxche qui va accueillir la fusion, puis lancer la commande avec le nom de la branche que vous voulez fusionner
```
git merge nom_de_la_branche_a_fusionner
```
